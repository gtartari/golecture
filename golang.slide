Go Language
Sep 2013

Giacomo Tartari
PhD student, University of Tromsø
giacomo.tartari@uit.no




* Go lecture 1/2

Introduction
Motivation
Syntax
Capabilities
Example code


* Go lectures 2/2

Practicalities
Installation
Environment
Tooling
HowTos
Demo?


* Go



* A new language?

Why? 
and what for?
Don't we have Java? 
C?
C++? 
C#?
D?
Haskell?
Scala?
Python? 
PHP? 
Ruby? 
Perl?!
Brainfuck!!!!
[add random language here]?

* A new language

What's wrong with all of the above?

.link http://talks.golang.org/2012/splash.article Rob Pike's take (one of the Go instigator)

Languages used at Google were not satisfactory

- These languages were developed before the multi-core revolution
- Millions of lines of code maintained by thousands of programmers
- Build times of many minutes or hours 

Go is compiled, concurrent, garbage-collected, statically typed


* Go

Modern and pragmatical language

Not a research language to explore new horizons

A language to get the job done

Designed by and for people who build and maintain large systems

Easy to read, clean syntax

Good tools

Nothing exactly new but a collection of good features


* Go

C-like syntax

Compiled to machine code

CSP-like Concurrency 

Garbage collected

Static and strong typed

No exceptions for handling errors

No inheritance but composition

No generics

No header files

.link http://golang.org/doc/faq



* Hello World


.play hello.go /START/,/STOP/



* Package

.code hello.go  /START/,/STOP/ HLpackage

Go packages mix the properties of libraries, name spaces, and modules

A package is compiled in a static library or in a (statically linked) executable if `main.main()` is present

Multiple files can be part of a package 

No restriction to what can be in a file (unlike Java)




* Importing other packages 

.code hello.go /START/,/STOP/ HLimport

Import statements are path to the package binary and/or source code

e.g. 

- import "encoding/json"
- import "net/http/cookiejar"
- import "github.com/golang/groupcache"
- import "code.google.com/p/go.talks/pkg/present"



* Function declaration

.code hello.go  /START/,/STOP/ HLmain

The func keyword is used to declare/define a function

	func Println(a ...interface{}) (n int, err error)

Functions are first-class

Functions can have more than one return value
#
#.code hello.go  /STARTF/,/STOPF/ HLmain

#Idiomatic code often looks like:

#`func` `fname(param)(stuff,` `error){...}`



* Output 

.code hello.go  /START/,/STOP/ HLfunc

From golang.org/pkg/fmt/

#	func Fprint(w io.Writer, a ...interface{}) (n int, err error)
#	func Fprintf(w io.Writer, format string, a ...interface{}) (n int, err error)
#	func Fprintln(w io.Writer, a ...interface{}) (n int, err error)
	...
	func Print(a ...interface{}) (n int, err error)
	func Printf(format string, a ...interface{}) (n int, err error)
	func Println(a ...interface{}) (n int, err error)
	func Scan(a ...interface{}) (n int, err error)
	func Scanf(format string, a ...interface{}) (n int, err error)
	func Scanln(a ...interface{}) (n int, err error)
	func Sprint(a ...interface{}) string
	func Sscan(str string, a ...interface{}) (n int, err error)
	...




* Basic types

	bool

	string

	int  int8  int16  int32  int64
	uint uint8 uint16 uint32 uint64 uintptr

	byte // alias for uint8

	rune // alias for int32
		 // represents a Unicode code point

	float32 float64

	complex64 complex128


Also array, slice, maps and channels
But we'll see them later





* Packages, exported identifiers

	package mypackage

	import (
		"errors"
		"fmt"
		"github.com/user/package"
	)

	var A int //exported

	func MyFunc(){...} //exported

	var b float32 //not exported

A name is visible outside its package iff

- The first character of the identifier's name is upper case
- The identifier is declared in the package block or it is a field name or method name.

Remember `fmt.Println(...)` in hello world?




* Varibles and Constants
	
	var (
		B       string = "hello"
		x, y, z float32
		p       *int
	)

	const (
		C   = iota //0
		D          //1
		E          //2
	)

As imports variables and constants can be declared in blocks

Variables, and constants, can be initialized when declared

Together with the iota constant generator it permits light-weight declaration of sequential values




* More varibles

Unused variables (and imports) are an error in Go

The blank identifier ( _ ) may be used in a declaration but the declaration does not introduce a new binding

Or in an assignment but the value is discarded

	_, _, _ = x, y, x

	const (
		_   = iota //0
		A          //1
		B          //2
	)

Inside functions variable can be defined with `:=`

	x := SomeFunc()
	y := x++
	x, y, z := 0, 1, 2

The compiler infers the type




#* Enums? Almost
#
#.code mypackage/mypackage.go  /STARTIOTA/,/STOPIOTA/
#
#The blank identifier ( _ ) may be used in a declaration but the declaration does not introduce a new binding


* Functions

- First class functions  
- Higher order functions
- User defined function types 
- Function literals 
- Closures



* Named return values

.play funcdiv.go  /START/,/STOP/

Return parameters can be named and treated as variables

A return statement without arguments returns the current values of the results


* More  functions 

.play firstfunc.go

First-class functions,  higher-order functions and user-defined function types


* More functions

.play closures.go

Function literals are closures


* Deferred functions

A defer statement schedules a function call to be run just before the function executing the defer returns

The canonical examples are unlocking a mutex or closing a file

	package main

	import (
		"fmt"
		"os"
	)

	func main() {
		fname := "/tmp/file.tmp"
		f, err := os.Open(fname)
		if err != nil {
			/*handle error*/
		}
		defer f.Close()

		// do stuff with file
	}


* Types, allocations and composition

- Type definition
- Method definition
- Allocation
- Interfaces
- Composition and embedding


* Type definition

	type myint int

	type User struct{
		Name string
		Age int
	}

	type Writer interface{
		Write()
	}


Go is strong typed so `MyInt` is not an `int`

Types can be onverted  with this syntax `int(MyInt)`

Any type can have methods (even functions)



* Allocation

Struct literals

	var (
		p = Vertex{1, 2}  // has type Vertex
		q = &Vertex{1, 2} // has type *Vertex
		r = Vertex{X: 1}  // Y:0 is implicit
		s = Vertex{}      // X:0 and Y:0
		t = Vertex{X: 3, Y: 4}
	)

Built in function `new()` returns a pointer to a newly allocated and zeroed memory

	v := new(Vertex) // has type *Vertex

Built in function `make()` used to allocate more complex built in types: channels, maps and slices

	m := make(map[string]int)

* Methods

	type Vertex struct {
		X, Y int
	}
	
	func (v1 Vertex)Add(v2 Vertex)Vertex{
		return Vector{v1.X + v2.X, v1.Y + v2.Y}
	}

Method invocation is with the dot notation

	var (
		m = Vertex{1, 2}
		n = Vertex{3, 4}
		)
	...
	m.Add(n)
    

* Method receivers

.play receiver.go 


* Interfaces

Interfaces are a sets of methods

Just behavior

Often just a few methods

From pkg/io

	type Writer interface {
		Write(p []byte) (n int, err error)
	}

	type Reader interface {
		    Read(p []byte) (n int, err error)
	}

	type Closer interface {
		Close() error
	}




* Interfaces

An interface is satisfied if the type implements all the methods in the set

No _implements_ keyword

Interface satisfaction is statically checked at compile time

Interfaces are type-safe

Structural typing, is like duck typing but better

The compiler tells you if it is a _duck_




* Interfaces

Any type satisfies the empty method set

	interface{}

.play iempty.go 




* Interfaces composition

Interfaces can be composed 

From pkg/io

	type ReadWriteCloser interface {
			Reader
			Writer
			Closer
	}

	func MyFunc(stream io.ReadWriteCloser) error{
		...
		stream.Read()
		sream.Write(data)
		stream.Close()
		...
	}


* Struct embedding 

Also struct can be composed by embedding

.play embed.go


* Struct embedding

And the interface they are satisfying as well
    
	type Locker interface {
			Lock()
			Unlock()
	}
    

	import "sync"

	type Vertex struct {
		X, Y int
	}

	type VertexLocker struct{
		sync.Mutex
		Vertex
	}

	vl := VertexLocker{}
	vl.Lock()
	vl.X = 99
	vl.Unlock()


* Flow Control: if, for and switch
#intermezzo reflection
#type switch 

* If 

.play if.go

* For

.play for.go

As in C or Java, you can leave the pre and post statements empty

And drop the semicolons: C's while is spelled for in Go

* Switch

.play switch.go


* Switch 

Not just numbers

.play switchstring.go

* Type assertion

	type error interface {
			Error() string
	}
    
    
	type PathError struct{
		Path string
		Ctx *Context
		Timestamp time.Time
	}
	
	func (pe PathError) Error() string{
		return fmt.Sprintf("Wrong path: %s", pe.Path)
	}
	
    
	err := FuncPath(...)
	ep, ok := err.(PathError) 
	if ok{
		ep.Ctx
	}



* Type switch

What is the actual type of an interface?

	err := json.Unmarshal(data, &p)
	if err != nil {
		switch t := err.(type) {
		case *json.UnmarshalFieldError:
			log.Println(t)
		case *json.UnmarshalTypeError:
			log.Println(t)
		case *json.UnsupportedTypeError:
			log.Println(t)
		case *json.UnsupportedValueError:
			log.Println(t)
		case *json.SyntaxError:
			log.Println(t)
		case *json.InvalidUnmarshalError:
			log.Println(t)
		}
		return err
	}


* Array, slice and maps 
#and channels



* Array


.play array.go




* Array 

Value type not reference type

The size of an array is part of its type

.play array2.go



* Slice

.play slice.go

Just a *slice* of an array


* Slice internals

.image godata3.png

.play slice2.go





* Maps

.play maps.go /START/,/STOP/

Keys can be integers, floats, complex, strings, pointers, interfaces, structs, arrays


* Range

.play range.go



* Concurrency: goroutines and channels


* Goroutine

.play goroutine.go

Goroutines live in the same address space

Think of them as avery lightweight threads
#We can use the sync package, but we have something better...



* Channels
	
	ch := make(chan int)	 // unbuffered channel
	ch := make(chan int, 10) // buffered channel

Communication primitives

	ch <- v                  // Send v to channel ch
	v := <-ch                // Receive from ch, and assign value to v


Unbuffered channels combine communication with synchronization

Buffered channel are more like synchronized and type safe FIFO queues

Same as Unix pipes

- Read blocks while pipe is empty
- Write blocks while pipe is full



* More channels

Channels can be closed to signal the receivers the termination of the data flow

	ch := make(chan int)
	close(ch)

To check if a channel is close use the multi-valued assignment form of the receive operator

	x, ok := <-ch
	if !ok{
		fmt.Println("Channel closed!")
	}

Receiving from a closed channel always succeeds, immediately returning the element type's zero value



* More channels

_Don't_communicate_by_sharing_memory;_share_memory_by_communicating._

	ch := make(chan *Message)
	...
	//producer
	go func(){
		for{
			ch <- &Message{...}
		}
	}()
	...
	//consumer
	go func(){
		for m := range ch {
			msg.Consume(m)
		}
	}()

The communication is the way these goroutines share memory

* Select

Select is similar to switch but each case is a communication statement
	
	in := make(chan int)
	out := make(chan int)

	select{
	case i := <- in:
		fmt.Println("received i")
	case out <- x:
		fmt.Println("sent x")
	default:
		fmt.Println("no communication")
	}

If `default` case is not present `select` blocks until a cahannel is ready to communicate

`select{}` blocks forever


* Select

.play select.go /START/,/STOP/

* Tricks

* Avoid (some) garbage

Buffered channel can hold resources to be reused

	var buffers = make(chan *Buffer, 100)

	go func(){
		var buff *Buffer
		select{
		case buff = <- buffers:
			//got one 
		default:
			buff = new(Buffer)
		}
	}
	...
	go func(){
		select{
		case buffers <- buff:
			//recycle 
		default:
			//garbage
		}
	}

* Broadcast a signal

Goroutines are cheap, can have 100000 running on normal hardware

Maybe the goroutines need to cleanup before the application shuts down

Keeping track of how many are alive can be difficult

But receiving from a closed channel always succeeds...
	
	var quit = make(chan *struct{})

	select{
	case buff := <-ch:
		//do stuff
	case <- quit:
		//shutdown, close files connections etc... 
		//cleanup
		//...
	}




* Instalaltion

Official binary distributions 

- FreeBSD
- Linux
- Mac OS X (Snow Leopard, Lion, and Mountain Lion)
- NetBSD
- Windows

Both 32 and 64 bit




* Installation

Or from source

- Needs a C compiler 
- Needs mercurial
- Allows you to try the delevopment brabch





* Environment

Some optionnal environment variables

- GOROOT
- GOOS
- GOARCH
- GOBIN
	
To override the defaults put sometinh like this in you `.bashrc` or `.profile`

	export GOROOT=$HOME/go
	export GOARCH=amd64
	export GOOS=linux
	export PATH=$GOROOT/bin:$PATH 

* Environment

One environment variable that is needed  is GOPATH

From the help:

	The Go path is used to resolve import statements.
	It is implemented by and documented in the go/build package.

	The GOPATH environment variable lists places to look for Go code.
	On Unix, the value is a colon-separated string.
	On Windows, the value is a semicolon-separated string.
	On Plan 9, the value is a list.

	GOPATH must be set to get, build and install packages outside the
	standard Go tree.

e.g. `GOPATH=/home/user/gocode`


* GOPATH


More from the help:

	Here's an example directory layout:

	GOPATH=/home/user/gocode

	/home/user/gocode/
		src/
			foo/
				bar/               (go code in package bar)
					x.go
				quux/              (go code in package main)
					y.go
		bin/
			quux                   (installed command)
		pkg/
			linux_amd64/
				foo/
					bar.a          (installed package object)


Also GOPATH/bin should go in your PATH


* Build constraints

A build constraint is a line comment beginning with _+build_ that lists the conditions under which a file should be included in the package

	// +build linux darwin
	// +build 386
	...
	// +build ignore

Or if the file name ends in

	*_GOOS
	*_GOARCH
	*_GOOS_GOARCH

E.g. `source_windows_amd64.go`


* go tool(s)

	$go help
	Go is a tool for managing Go source code.

	Usage:

		go command [arguments]

	The commands are:

		build       compile packages and dependencies
		clean       remove object files
		env         print Go environment information
		fix         run go tool fix on packages
		fmt         run gofmt on package sources
		get         download and install packages and dependencies
		install     compile and install packages and dependencies
		list        list packages
		run         compile and run Go program
		test        test packages
		tool        run specified go tool
		version     print Go version
		vet         run go tool vet on packages
		...

* go tool(s)

go get 

	$go get github.com/golang/glog

go build

	$cd $GOPATH/src/myproject
	$go build 

go run

	$cd $GOPATH/src/myproject
	$vim main.go
	$go run main.go

go fmt (aka end of coding style war!!)

	$go fmt .

* godoc 

Offline docs

	$godoc
	usage: godoc package [name ...]
	godoc -http=:6060
	...

Online docs for the standard library

.link http://golang.org/pkg

Online docs for third party libraries

.link http://godoc.org/
.link http://gowalker.org/

Online presentation

.link http://talks.godoc.org/


* Integration

$ls -l $GOROOT/misc

	IntelliJIDEA
	arm
	bash
	bbedit
	benchcmp
	cgo
	chrome
	dashboard
	dist
	emacs
	fraise
	git
	goplay
	kate
	linkcheck
	notepadplus
	pprof
	swig
	vim
	xcode
	zsh



* Let's code

How to write a Go package (library) step by step

Create a directory 

Write the code (and the documentation and the tests)

Compile it

Run the tests

See the docs off line


* Test


* Benchmark


* Readings


http://golang.org/
http://talks.golang.org/2012/splash.article
http://golang.org/doc/effective_go.htm
http://golang.org/ref/spec
https://code.google.com/p/go-wiki/w/list
https://code.google.com/p/go-wiki/wiki/GoTalks
http://blog.golang.org/
http://morsmachine.dk/go-scheduler
http://swtch.com/~rsc/thread/
http://www.cs.ucf.edu/courses/cop4020/sum2009/CSP-hoare.pdf
http://concur.rspace.googlecode.com/hg/talk/concur.html
http://talks.golang.org/2012/concurrency.slide
http://talks.golang.org/2013/advconc.slide
http://research.swtch.com/gotour
http://research.swtch.com/godata
https://gobyexample.com/
http://learnxinyminutes.com/docs/go/
http://tour.golang.org/
https://groups.google.com/forum/#!forum/golang-nuts
https://plus.google.com/communities/114112804251407510571/stream/f49df777-7381-4c40-b547-44605e01a866
http://golang.org/doc/articles/race_detector.html
http://godoc.org/




